/*
1. Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.
    Javascript являється однопоточною мовою, що означає виконання коду в порядку черговості зверху до низу по рядках (синхронний код).
    Однак іноді існує потреба виконувати код з певною затримкою (очікування відповіді від сервера, таймаути). 
    Для цього використовуються проміси та асинхронні функції, що дозволяє "дочекатися" виконання потрібної дії та продовжити роботу коду в залежності від її результатів
*/

"use strict";

const getIpService = 'https://api.ipify.org/?format=json';
const getAdressService = (ip) => `http://ip-api.com/json/${ip}?fields=status,message,continent,country,region,regionName,city,district`;

const findBtn = document.querySelector('#find-btn');
const root = document.querySelector('.info');

// функція-запит
const sendRequest = async (API) => {
  return await fetch(API).then(response => {
      if(response.ok) {
          return response.json();
      } else {
          return new Error('Something goes wrong');
      }
  })
}

const getIP = () => sendRequest(getIpService);
const getAdress = (ip) => sendRequest(getAdressService(ip));


const showInfo = ({continent, country, regionName, city, district}) => {
  root.innerHTML = '';
  root.insertAdjacentHTML('afterbegin', `
  <dl>
    <dt>Continent:</dt>
    <dd>${continent}</dd>
    <dt>Country:</dt>
    <dd>${country}</dd>
    <dt>Region:</dt>
    <dd>${regionName}</dd>
    <dt>City:</dt>
    <dd>${city}</dd>
    <dt>District:</dt>
    <dd>${district}</dd>
  </dl>
  `)
}


findBtn.addEventListener('click', async () => {
  let IP;

  try {
    await getIP().then(data => {
      IP = data.ip;
    });
  } catch(e) {
    console.log(e.message);
  }

  try {
    await getAdress(IP).then(data => {
      if(data.status === 'success') {
        showInfo(data);
      } else {
        throw new Error(data.message);
      }
    })
  } catch(e) {
    console.log(e.message);
  }

})